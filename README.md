# Konverze ruznych vstupnich souboru

## GDAL - program ke konverzi ruznych datovych souboru

K uspesnemu behu je treba nainstalovat [GDAL](https://gdal.org/) a mit pristupny prikaz
`ogr2ogr`. Na webove strance je navod jak nainstalovat, na Debian-like distribucich staci

```bash
apt-get -y install gdal-bin gdal-data
```

## Jak spustit vsechny importy

Vsechny importy lze spustit pomoci

```bash
yarn install
yarn convert
```

Pro import jen konkretniho souboru, spustit

```bash
yarn convert -s ./specs/de_berlin.spec.json
```

Konverze zalozi podadresar `work` a v nem jednotlive adresare pro kazdy konkretni spec file. Momentalne konci
tim, ze vytvori soubory jeden zaznam na jednu radku `output.trees` a `output.treeowners`.

## Jak probiha konverze

### Krok 1: vybaleni

Vstupni soubory jsou v ruznych formatech a pro GDAL je je potencialne treba vybalit.
Momentalne podporujeme:

- primy vstupni soubor/soubory
- zazipovane vstupni soubory

Tato faze konverze je specifikovana pomoci jednoducheho JSONu:

```json
{
  "source": {
    "href": "https://www.mesto.cz/stromy.zip",
    "process": [ { "unzip": true } ],
    "file": "shp230/ps_zelen_stromy16_gs_pnt.shp"
  }
}
```

Parametr `process` je nepovinny a momentalne podporuje pouze krok `unzip`.

Parametr `file` je nepovinny, pouziva se pouze pri rozbaleni
a specifikuje jmeno souboru, ze ktereho se budou nacitat vlastni
data.

### Krok 2: konverze do GeoJSON feature sequence file

Abychom nemuseli nacitat cely JSON najednou, pouzivame vystupni format GDALu, ktery
sype jednotlive GeoJSON features ven jednu na jednu radku. GDAL mu rika `GeoJSONSeq`.

Pri konverzi se konvertuji nejenom formaty souboru, ale i souradnicove systemy a
kodovani cestiny (ci jinych jazyku)

Dulezite argumenty pro `ogr2ogr`:

- `-f GeoJSONSeq` specifikuje vystupni format, pro nas vzdy GeoJSONSeq
- `-oo ENCODING=...` specifikace vstupniho encodingu, pouziva se u Shape files (pokud nejsou v UTF-8).
- `-s_srs ...` zdrojovy koordinatovy system (pokud neni GPS/WGS84/EPSG:4326).
- `-t_srs ...` cilovy koordinatovy system, je treba specifikovat pokud je uveden `-s_srs`, vzdy EPSG:4326.
- `-nlt POINT` prekonvertuje vsechny features na POINT, cimz normalizuje vyzvedavani dat pote (nektere vstupni soubory maji MULTIPOINT).

Priklady:

- ```bash
  ogr2ogr -f GeoJSONSeq -nlt POINT \
    -oo ENCODING=windows-1250 \
    -s_srs EPSG:5514 \
    -t_srs EPSG:4326
  ```

  prekonvertuje vstupni soubor z kodovani windows1250 a koordinatoveho systemu EPSG:5514

Tento krok je popsany pomoci jednoducheho JSONu:

```json
{
  "gdal": {
    "args": [ "-oo", "ENCODING=windows-1250" ]
  }
}
```

Z tohoto kroku vznika soubor `extracted.geojsonseq`.

### Krok 3: vyzvednuti parametru ktere konvertujeme do Zalejme

Jednotliva radka GeoJSONSeq souboru je jedna GeoJSON featura, ktera ma nasledujici format:

```json
{
  "type": "Feature",
  "properties": {
    "jmeno_property": "hodnota",
    "dalsi_property": "jina_hodnota",
    "ciselna_property": 123
  },
  "geometry": {
    "type": "Point",
    "coordinates": [ 16.62743837, 49.19895352 ]
  }
}
```

Polohu stromu lze vyzvednout vzdy z `geometry.coordinates`, kde je v poradi *delka* (*longitude*), *sirka* (*latitude*). Nase atributy se pote naplni (pokud jsou ve zdrojovem souboru uvedeny) z prislusnych properties. Toto mapovani je specifikovano v tzv. crosswalk, ktery
je cely popsan v [types.ts](types.ts) jako typ `SourceSpecCrosswalk`. Lze provadet ruzne drobne konverze jako
konverze jednotek, prevod obvodu koruny na prumer, atp...

```json
{
  "crosswalk": {
    "taxonLatin": { "property": "LATIN" },
    "taxonCzech": { "property": "CZECH" },
    "height": { "property": "VYSKA" },
    "id": { "property": "ID" },
  }
}
```

Kde zdrojem muze byt budto property z featury specifikovana jako
`{ "property": "jmeno_property" }` a nebo lze zapsat konstantni hodnotu pomoci `{ "constant": "..." }`. Toto se tyka predevsim tech pripadu, kdy v souboru dana data nejsou a napr. spravce stromu je jasny uz primo ze souboru.

Z tohoto kroku konverze vystupuji dva soubory:

- `output.trees` seznam stromu pripraveny k nacteni do DB
- `output.treeowners` seznam spravcu stromu pripraveny k nacteni do DB

### Definicni soubor importu

Cely definicni soubor importu pak vypada napr. nasledovne:

```json
{
  "source": {
    "href": "https://www.mesto.cz/stromy.zip",
    "process": [ { "unzip": true } ],
    "file": "shp230/ps_zelen_stromy16_gs_pnt.shp"
  },
  "gdal": {
    "args": [ "-oo", "ENCODING=windows-1250" ]
  },
  "crosswalk": {
    "taxonLatin": { "property": "LATIN" },
    "taxonCzech": { "property": "CZECH" },
    "height": { "property": "VYSKA" },
    "id": { "property": "ID" },
    "ownerName": { "constant": "Město Tišnov" }
  }
}
```
