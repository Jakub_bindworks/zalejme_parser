export interface TreeRecord {
  id: string;
  ownerId: string;
  ownerName: string;
  cityName?: string;
  districtName?: string;
  taxonLatin?: string;
  taxonCzech?: string;
  detailedLocation?: string;
  note?: string;
  height?: number;
  hasWateringHole?: boolean;
  crownDiameter?: number;
}

export interface TreeOwnerRecord {
  id: string;
  name: string;
}

export interface GeoJsonFeature {
  type: 'Feature';
  properties: {
    [name: string]: string | number;
  };
  geometry: {
    type: 'Point';
    coordinates: [number, number];
  };
}

export interface SourceSpec {
  source: SourceSpecSource;
  gdal: SourceSpecGdal;
  crosswalk: SourceSpecCrosswalk;
}

export interface SourceSpecSource {
  href: string;
  process?: SourceSpecSourceProcess[];
  file?: string;
}

export type SourceSpecSourceProcess = SourceSpecSourceProcessUnzip;

export interface SourceSpecSourceProcessUnzip {
  unzip: true;
}

export interface SourceSpecGdal {
  args?: string[];
}

export interface SourceSpecCrosswalk {
  cityName: SourceSpecCrosswalkSource;
  ownerName: SourceSpecCrosswalkSource;

  id?: SourceSpecCrosswalkSource;
  taxonCzech?: SourceSpecCrosswalkSource;
  taxonLatin?: SourceSpecCrosswalkSource;
  hasWateringHole?: SourceSpecCrosswalkSource;
  ownerId?: SourceSpecCrosswalkSource;
  height?: SourceSpecCrosswalkSource;
  crownDiameter?: SourceSpecCrosswalkSource;
  detailedLocation?: SourceSpecCrosswalkSource;
  note?: SourceSpecCrosswalkSource;
  districtName?: SourceSpecCrosswalkSource;
}

export type SourceSpecCrosswalkSource =
  | SourceSpecCrosswalkSourceProperty
  | SourceSpecCrosswalkSourceConstant;

export interface SourceSpecCrosswalkSourceProperty {
  /**
   * Name of the property to extract value from.
   * If multiple properties are specified, their values are joined together into one string.
   */
  property: string | string[];

  /**
   * Run the specified regular expression on the result and only extract the $1 captured value.
   */
  regexp?: string;

  /**
   * Convert circumference to diameter.
   */
  circumference2diameter?: true;

  /**
   * Multiply number by a constant (for example to convert units).
   */
  multiply?: number;
}

export interface SourceSpecCrosswalkSourceConstant {
  /**
   * Extract constant value
   */
  constant: string | number | boolean;
}
