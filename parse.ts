import axios from 'axios';
import child from 'child_process';
import fs from 'fs';
import glob from 'glob';
import ngeohash from 'ngeohash';
import path from 'path';
import readline from 'readline';
import rimraf from 'rimraf';
import { URL } from 'url';
import util from 'util';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import {
  GeoJsonFeature,
  SourceSpec,
  SourceSpecCrosswalk,
  SourceSpecCrosswalkSource,
  SourceSpecGdal,
  SourceSpecSourceProcess,
  TreeOwnerRecord,
  TreeRecord,
} from './types';
import { string2boolean, waitForStreamFinish } from './utils';

// eslint-disable-next-line prefer-destructuring
const argv = yargs(hideBin(process.argv)).option('spec', {
  alias: 's',
  type: 'string',
  description: 'Specification file name(s), can be a glob pattern',
  default: './specs/*.spec.json',
}).argv;

export interface SourceSpecFile {
  file: string;
  id: string;
  workDir: string;
  spec: SourceSpec;
}

async function loadSpecFiles(globPattern: string): Promise<SourceSpecFile[]> {
  const pglob = util.promisify(glob);
  const pathNames = await pglob(globPattern);
  return pathNames.map((pathName) => {
    const id = path.basename(pathName).replace(/\.spec.json$/, '');
    return {
      file: pathName,
      id,
      workDir: `./work/${id}`,
      spec: JSON.parse(fs.readFileSync(pathName, { encoding: 'utf-8' })),
    };
  });
}

/** STEP1: Download and unpack ************************************************************** */

async function downloadFile(href: string, workDir: string): Promise<string> {
  if (href.startsWith('./')) {
    const baseName = path.basename(href);
    fs.copyFileSync(href, path.join(workDir, baseName));
    return baseName;
  }

  const url = new URL(href);
  const baseName = path.basename(url.pathname);
  const writer = fs.createWriteStream(path.join(workDir, baseName));
  const response = await axios.get(href, {
    responseType: 'stream',
  });
  response.data.pipe(writer);
  await waitForStreamFinish(writer);
  return baseName;
}

async function processFile(
  fileName: string,
  workDir: string,
  spec: SourceSpecSourceProcess,
): Promise<void> {
  if (spec.unzip) {
    console.info(`Unzipping ${fileName}...`);
    child.execSync(`unzip "${fileName}"`, { cwd: workDir, encoding: 'ascii' });
  }
}

async function downloadFileAndUnpack(
  specFile: SourceSpecFile,
): Promise<string> {
  if (fs.existsSync(specFile.workDir)) {
    console.info(`Deleting working directory ${specFile.workDir}...`);
    await util.promisify(rimraf)(specFile.workDir);
  }

  console.info(`Creating working directory ${specFile.workDir}...`);
  await util.promisify(fs.mkdir)(specFile.workDir, { recursive: true });

  console.info(`Downloading source file ${specFile.spec.source.href}...`);
  const fileName = await downloadFile(
    specFile.spec.source.href,
    specFile.workDir,
  );

  if (specFile.spec.source.process) {
    await specFile.spec.source.process.reduce(
      (p, process) =>
        p.then(() => {
          processFile(fileName, specFile.workDir, process);
        }),
      Promise.resolve(),
    );
  }

  return specFile.spec.source.file ?? fileName;
}

/** STEP2: GDAL translation ***************************************************************** */

async function translateFileWithGdal(
  fileName: string,
  workDir: string,
  spec: SourceSpecGdal,
): Promise<void> {
  const gdalCommand: string[] = [
    'ogr2ogr',
    '-f',
    'GeoJSONSeq',
    '-nlt',
    'POINT',
  ];

  if (spec.args) {
    gdalCommand.push(...spec.args);
  }

  gdalCommand.push('extracted.geojsonseq');
  gdalCommand.push(fileName);

  const gdalCommandString = `"${gdalCommand.join('" "')}"`;

  console.info(`Running GDAL: ${gdalCommandString}...`);
  child.execSync(gdalCommandString, { cwd: workDir, encoding: 'ascii' });
}

/** STEP3: Properties extraction ************************************************************ */

function extractProperty(
  feature: GeoJsonFeature,
  valueSource: SourceSpecCrosswalkSource,
  type: 'string',
): string;
function extractProperty(
  feature: GeoJsonFeature,
  valueSource: SourceSpecCrosswalkSource,
  type: 'number',
): number;
function extractProperty(
  feature: GeoJsonFeature,
  valueSource: SourceSpecCrosswalkSource,
  type: 'boolean',
): boolean;
function extractProperty(
  feature: GeoJsonFeature,
  valueSource: SourceSpecCrosswalkSource | undefined,
  type: 'string',
): string | undefined;
function extractProperty(
  feature: GeoJsonFeature,
  valueSource: SourceSpecCrosswalkSource | undefined,
  type: 'number',
): number | undefined;
function extractProperty(
  feature: GeoJsonFeature,
  valueSource: SourceSpecCrosswalkSource | undefined,
  type: 'boolean',
): boolean | undefined;
function extractProperty(
  feature: GeoJsonFeature,
  valueSource: SourceSpecCrosswalkSource | undefined,
  type: 'string' | 'number' | 'boolean',
): string | number | boolean | undefined {
  if (!valueSource) {
    return undefined;
  }

  function extractPropertyValue(property: string | string[]): string | number {
    if (typeof property === 'string') {
      return feature.properties[property];
    }

    return property.map((name) => String(feature.properties[name])).join(' ');
  }

  function extractValue(): string | number | boolean | undefined {
    const value =
      // eslint-disable-next-line no-nested-ternary
      'property' in valueSource
        ? extractPropertyValue(valueSource.property)
        : 'constant' in valueSource
        ? valueSource.constant
        : undefined;

    if (!value) {
      return undefined;
    }

    switch (type) {
      case 'string':
        return String(value);
      case 'number': {
        if (typeof value === 'number') {
          return value;
        }
        const flt = parseFloat(String(value));
        return Number.isNaN(flt) ? undefined : flt;
      }
      case 'boolean':
        return typeof value === 'boolean'
          ? value
          : string2boolean(String(value));
      default:
        return undefined;
    }
  }

  function processStringValue(value: string) {
    if ('regexp' in valueSource) {
      return (new RegExp(valueSource.regexp).exec(value) ?? ['', ''])[1];
    }
    return value;
  }

  function processNumberValue(value: number) {
    let newValue = value;
    if ('circumference2diameter' in valueSource) {
      newValue = (newValue / Math.PI) * 2;
    }
    if ('multiply' in valueSource) {
      newValue *= valueSource.multiply;
    }
    return newValue;
  }

  const extractedValue = extractValue();

  if (extractedValue === undefined) {
    return undefined;
  }

  switch (typeof extractedValue) {
    case 'string':
      return processStringValue(extractedValue);

    case 'number':
      return processNumberValue(extractedValue);

    default:
      return extractedValue;
  }
}

async function generateTreesAndOwners(
  idPrefix: string,
  workDir: string,
  crosswalk: SourceSpecCrosswalk,
): Promise<void> {
  console.info(`Extracting properties...`);

  const rl = readline.createInterface({
    input: fs.createReadStream(path.join(workDir, 'extracted.geojsonseq')),
    crlfDelay: Infinity,
  });

  const seenOwners: { [id: string]: true } = {};

  const treesFile = fs.createWriteStream(path.join(workDir, 'output.trees'), {
    encoding: 'utf-8',
  });

  const treesOwnersFile = fs.createWriteStream(
    path.join(workDir, 'output.treeowners'),
    {
      encoding: 'utf-8',
    },
  );

  // eslint-disable-next-line no-restricted-syntax
  for await (const line of rl) {
    const feature: GeoJsonFeature = JSON.parse(line);

    const latitude = feature.geometry.coordinates[1];
    const longitude = feature.geometry.coordinates[0];
    const id = ngeohash.encode(latitude, longitude, 11);
    const ownerName =
      extractProperty(feature, crosswalk.ownerName, 'string') ?? 'Neznámý';
    const ownerId =
      idPrefix +
      (extractProperty(feature, crosswalk.ownerId, 'string') ?? ownerName);
    const cityName = extractProperty(feature, crosswalk.cityName, 'string');
    const districtName =
      extractProperty(feature, crosswalk.districtName, 'string') ?? cityName;
    const taxonLatin = extractProperty(feature, crosswalk.taxonLatin, 'string');
    const taxonCzech = extractProperty(feature, crosswalk.taxonCzech, 'string');
    const hasWateringHole = extractProperty(
      feature,
      crosswalk.hasWateringHole,
      'boolean',
    );
    const height = extractProperty(feature, crosswalk.height, 'number');
    const crownDiameter = extractProperty(
      feature,
      crosswalk.crownDiameter,
      'number',
    );
    const detailedLocation = extractProperty(
      feature,
      crosswalk.detailedLocation,
      'string',
    );
    const note = extractProperty(feature, crosswalk.note, 'string');

    const treeRecord: TreeRecord = {
      id,
      ownerId,
      ownerName,
      cityName,
      districtName,
      taxonLatin,
      taxonCzech,
      detailedLocation,
      note,
      height,
      hasWateringHole,
      crownDiameter,
    };

    treesFile.write(`${JSON.stringify(treeRecord)}\n`);

    if (!seenOwners[ownerId]) {
      seenOwners[ownerId] = true;

      const treeOwnerRecord: TreeOwnerRecord = {
        id: ownerId,
        name: ownerName,
      };

      treesOwnersFile.write(`${JSON.stringify(treeOwnerRecord)}\n`);
    }
  }

  treesFile.end();
  treesOwnersFile.end();
}

/** MAIN ************************************************************************************ */

async function run(): Promise<void> {
  const specs = await loadSpecFiles(argv.spec);

  // eslint-disable-next-line no-restricted-syntax
  for (const spec of specs) {
    // eslint-disable-next-line no-await-in-loop
    const fileName = await downloadFileAndUnpack(spec);

    // eslint-disable-next-line no-await-in-loop
    await translateFileWithGdal(fileName, spec.workDir, spec.spec.gdal);

    // eslint-disable-next-line no-await-in-loop
    await generateTreesAndOwners(
      `${spec.id}:`,
      spec.workDir,
      spec.spec.crosswalk,
    );
  }
}

run().catch((e) => {
  console.error(`Error occured`);
  console.error(e);
  process.exit(1);
});
