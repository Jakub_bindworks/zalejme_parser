import stream from 'stream';

export function waitForStreamFinish(aStream: stream.Stream): Promise<void> {
  return new Promise((resolve, reject) => {
    aStream.on('finish', resolve);
    aStream.on('error', reject);
  });
}

export function string2boolean(str: string): boolean {
  return ['A', 'Ano', 'Y', 'Yes', 'J', 'Ja', '1'].indexOf(str) > -1;
}
